<?php

namespace app\modules\back\adminuser;

use app\modules\back\BackendModule;

class Module extends BackendModule
{

    public $controllerNamespace = 'app\modules\back\adminuser\controllers';


    public function init()
    {
        parent::init();
    }
}
