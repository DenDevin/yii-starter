<?php

namespace app\modules\back\admin\models;

use Yii;

/**
 * This is the model class for table "proxy_server".
 *
 * @property int $id
 * @property string $ip
 * @property string $port
 * @property string|null $password
 * @property string|null $country
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class ProxyServer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proxy_server';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip', 'port'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['ip', 'port', 'password', 'country'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'port' => 'Port',
            'password' => 'Password',
            'country' => 'Country',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
