<?php

namespace app\modules\back\admin\models;

use Serps\Core\Browser\Browser;
use Serps\Core\Http\Proxy;
use Serps\HttpClient\CurlClient;
use Serps\SearchEngine\Google\GoogleClient;
use Serps\SearchEngine\Google\GoogleUrl;
use Yii;
use app\modules\back\admin\models\ProxyServer;
use yii\httpclient\Client;

/**
 * This is the model class for table "google".
 *
 * @property int $id
 * @property string|null $query
 * @property string|null $ads_block
 * @property int|null $pages_count
 * @property string|null $title
 * @property int|null $position
 * @property string|null $image
 * @property string|null $description
 * @property string|null $settings
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Google extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'google';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ads_block', 'description', 'settings'], 'string'],
            [['pages_count', 'position', 'status', 'created_at', 'updated_at'], 'integer'],
            [['query', 'title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'query' => 'Query',
            'ads_block' => 'Ads Block',
            'pages_count' => 'Pages Count',
            'title' => 'Title',
            'position' => 'Position',
            'image' => 'Image',
            'description' => 'Description',
            'settings' => 'Settings',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }



    public static function parseGoogleData($post)
    {



        $proxiesList = [
            "117.102.77.3:8080",
            "114.199.115.244:38509",
            "114.7.164.182:8080",
            "114.199.123.194:8080",
            "114.6.87.177:60811",
            "114.57.49.66:53281",
            "150.107.143.149:9797",
            "103.109.2.42:8080",
            "117.102.73.44:8182",
            "116.0.2.162:45054",
            "103.105.80.13:8080",
            "103.111.219.183:8118",
            "103.110.89.243:8080",
            "117.102.104.131:31796",
            "117.102.78.42:8080",
            "150.107.143.129:9797",
            "103.111.55.58:18533",
            "146.196.108.202:80",
            "103.111.55.58:64278",
            "101.255.40.18:34367",
            "101.255.125.10:8080",
            "103.105.67.158:8080",
            "103.20.189.126:3128",
            "103.105.77.15:8080",
            "103.123.64.234:3128",
            "103.111.55.58:2020",
            "49.128.181.229:8080",
            "103.106.114.134:8080",
            "103.111.218.23:8118",
            "118.99.118.86:23500",
            "103.142.224.19:80",
            "103.111.31.230:8080",
            "103.111.55.58:155",
            "118.99.124.57:8080",
            "103.143.20.76:8080",
            "103.143.20.75:8080"

        ];
        $randProxies = array_rand($proxiesList, 1);



        $userAgentsList = [
            'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2866.71 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:77.0) Gecko/20190101 Firefox/77.0',
            'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:77.0) Gecko/20100101 Firefox/77.0',
            'Mozilla/5.0 (X11; Linux ppc64le; rv:75.0) Gecko/20100101 Firefox/75.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/75.0',
            'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.10; rv:75.0) Gecko/20100101 Firefox/75.0',
            'Mozilla/5.0 (X11; Linux; rv:74.0) Gecko/20100101 Firefox/74.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0) Gecko/20100101 Firefox/73.0',
            'Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2919.83 Safari/537.36',


        ];

        $randAgents = array_rand($userAgentsList);
        $browserLanguage = "ru-RU";
        $arr = [];

            for($ii = 1; $ii <= $post['quantity']; $ii++)
            {
                $httpProxy = Proxy::createFromString($proxiesList[$randProxies[0]]);
                $browser = new Browser(new CurlClient(), $userAgentsList[$randAgents], $browserLanguage, null, $httpProxy);
                $googleClient = new GoogleClient($browser);
                $googleUrl = new GoogleUrl();
                $googleUrl->setSearchTerm($post['query']);
                $googleUrl->setPage($ii);

                $response = $googleClient->query($googleUrl);
                $results = $response->getNaturalResults();

                foreach($results as $result)
                {
                    $arr[$ii]['title'] = $result->title;
                    $arr[$ii]['url'] = $result->url;
                    $arr[$ii]['description'] = $result->description;
                    $arr[$ii]['position'] = $result->getRealPosition();
                }
            }

            return $arr;

    }


}
