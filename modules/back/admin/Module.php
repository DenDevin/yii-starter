<?php

namespace app\modules\back\admin;

use Yii;
use yii\filters\AccessControl;
use app\modules\back\BackendModule;
/**
 * admin module definition class
 */
class Module extends BackendModule
{
    public $controllerNamespace = 'app\modules\back\admin\controllers';
}
