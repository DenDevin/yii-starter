<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\back\admin\models\ProxyServer */

$this->title = 'Create Proxy Server';
$this->params['breadcrumbs'][] = ['label' => 'Proxy Servers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proxy-server-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
