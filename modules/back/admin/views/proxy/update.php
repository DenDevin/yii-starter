<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\back\admin\models\ProxyServer */

$this->title = 'Update Proxy Server: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proxy Servers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proxy-server-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
