<?php

  use yii\helpers\Html;

?>



<div class="col-lg-6">
    <?= Html::beginForm(['google/index'], 'post', ['enctype' => 'multipart/form-data']) ?>
    <div class="form-group">
        <label>Search query:</label>
        <?= Html::input('text', 'query', '', ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <label>Quantity results:</label>
        <?= Html::input('text', 'quantity', '10', ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>
    <?= Html::endForm() ?>
</div>
<div class="col-lg-6">

</div>
    <div class="col-lg-12">
        <? if ($data) : ?>
        <? $i = 1; ?>
            <?php foreach ($data as $dataItem):   ?>
                <div class="row panel">
                    <div class="panel-body">
                        <div class="col-sm-1 panel-heading"><?= $i ?> </div>
                        <div class="col-sm-9 panel-heading"><?=$dataItem['title']?> </br></br>
                            <code><?=$dataItem['url']?></code></br></br>
                            <?=$dataItem['description']?>
                        </div>
                        <div class="col-sm-2"><?=$dataItem['position']?></div>
                    </div>
                </div>
                <?php $i++; ?>
                <?php endforeach; ?>
        <?php endif; ?>
    </div>
