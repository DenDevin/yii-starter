<?php

namespace app\modules\back;

use Yii;
use yii\filters\AccessControl;
/**
 * admin module definition class
 */
class BackendModule extends \yii\base\Module
{

    public $layout = '@app/modules/back/views/layouts/main';

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                ],
            ],
        ];
    }


    public function init()
    {
        parent::init();


    }
}
