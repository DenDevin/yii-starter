<?php

namespace app\modules\back\adminrole;

use app\modules\back\BackendModule;

class Module extends BackendModule
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\back\adminrole\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
