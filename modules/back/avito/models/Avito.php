<?php

namespace app\modules\back\avito\models;

use yii\base\Model;
use Goutte\Client;

class Avito extends Model
{

    public static $url = 'https://www.avito.ru/rossiya';
    public static $data = [];

    public static function getMainPageData()
    {

        $data = [];
        $client = new Client();
        $crawler = $client->request('GET', self::$url);
        print_r($crawler); die;
        $regexp = '//div[contains(@class, "category-with-counters-item")]/a[contains(@class, "link-link")]';
        self::getXPathData($crawler, $regexp, 'mainCategory');
        return self::$data;
    }

    public static function getXPathData($crawler, $regexp, $attr)
    {

        $crawler->filterXPath($regexp)->each(function ($node)
            use ($attr)
        {

            self::$data[$attr]['url'][] = $node->attr('href');
            self::$data[$attr]['title'][] = $node->text();

        });

    }
}