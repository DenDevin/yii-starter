<?php

namespace app\modules\back\avito;

use app\modules\back\BackendModule;

class Module extends BackendModule
{

    public $controllerNamespace = 'app\modules\back\avito\controllers';

    public function init()
    {
        parent::init();
    }
}
