<?php

namespace app\modules\back\avito\controllers;


use yii\web\Controller;
use app\modules\back\avito\models\Avito;


class DefaultController extends Controller
{

    public function actionIndex()
    {

       $crawler = Avito::getMainPageData();

       return $this->render('index', [
            'crawler' => $crawler
        ]);
    }
}
