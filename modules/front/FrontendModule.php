<?php

namespace app\modules\front;

use Yii;
use yii\filters\AccessControl;
/**
 * admin module definition class
 */
class FrontendModule extends \yii\base\Module
{

    public $layout = 'main';

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                ],
            ],
        ];
    }


    public function init()
    {
        parent::init();


    }
}
