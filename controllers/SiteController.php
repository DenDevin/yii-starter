<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Projects;
use app\models\search\ProjectsSearch;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Json;

class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionExport()
    {




           $projects = Projects::find()->asArray()->all();



             foreach($projects as $key => $project)
              {
                  $celData = [
                      0 => [
                          [
                              'id',
                              'title',
                              'description',
                              'organization',
                              'start',
                              'end'

                          ],

                      ]
                  ];

                 if($key == 0)
                 {
                     $cellData[$key+1] = [


                         0 => $project->id,
                         1 => $project->title,
                         2 => $project->description,
                         3 => $project->organization,


                         4 => $project->start,
                         5 => $project->end,

                        /*
                         6 => $project->role,
                         7 => $project->link,
                         8 => $project->skills,
                         9 => $project->attachments,
                         10 => $project->type,
                         11 => $project->active,
                         12 => $project->sort,
                         13 => $project->updated_at,
                         14 => $project->created_at
                         */


                     ];


                 }

                 else

                     {
                         $cellData[$key] = [


                             0 => $project->id,
                             1 => $project->title,
                             2 => $project->description,
                             3 => $project->organization,


                             4 => $project->start,
                             5 => $project->end,


                             /*
                             6 => $project->role,
                             7 => $project->link,
                             8 => $project->skills,
                             9 => $project->attachments,
                             10 => $project->type,
                             11 => $project->active,
                             12 => $project->sort,
                             13 => $project->updated_at,
                             14 => $project->created_at
                             */

                         ];
                     }
                  }



        Yii::$app->excel->write($cellData);
        Yii::$app->excel->store('projects', 'xls', './docs/excel/');
        $this->redirect('/docs/excel/projects.xls');
    }


    public function actionImage()
    {

         $model = new Projects();
         $someFiles = UploadedFile::getInstances($model, 'attachments');
        $directory = Yii::getAlias('@web/files') . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }


        if ($someFiles) {

            foreach($someFiles as $someFile)
            {

                $uid = uniqid(time(), true);
                $fileName = $uid . '.' . $someFile->extension;
                $filePath = $directory . $fileName;
                if ($someFile->saveAs($filePath)) {
                    $path = '/images/projects/' . Yii::$app->session->id . DIRECTORY_SEPARATOR . $fileName;
                    return Json::encode([
                        'files' => [
                            [
                                'name' => $fileName,
                                'size' => $someFile->size,
                                'url' => $path,
                                'thumbnailUrl' => $path,
                                'deleteUrl' => 'image-delete?name=' . $fileName,
                                'deleteType' => 'POST',
                            ],
                        ],
                    ]);
                }
            }


        }

        return '';





    }


    public function actionUpload()
    {
        $transData  = [];

        $model = new ProjectsSearch();
        $importFile = UploadedFile::getInstance($model, 'xml_file');
        $rawDatas = Yii::$app->excel->read($importFile->tempName);
        $ids = [];
        foreach($rawDatas as  $sheetName => $rawData)
        {
          foreach($rawData as $raw)
          {
              $id = (int)$raw[0];
              $ids[] = $id;
              $isset = Projects::findOne($id);
              if(!$isset)
              {

                  if(!empty($id) && is_integer($id) )
                  {



                      $modelProject = new Projects([
                          'id' => $id,
                          'title' => $raw[1],
                          'description' => $raw[2],
                          'organization' => $raw[3],
                          'start' => $raw[4],
                          'end' => $raw[5],
                          'role' => $raw[6],
                          'link' => $raw[7],
                          'skills' => $raw[8],
                          'attachments' => $raw[9],
                          'type' => $raw[10],
                          'active' => $raw[11],
                          'sort' => $raw[12],
                          'updated_at' => $raw[13],
                          'created_at' => $raw[14],

                      ]);

                      $modelProject->save(false);
                  }

              }

              else
                  {

                     $isset->title = $raw[1];
                     $isset->description = $raw[2];
                      $isset->organization = $raw[3];
                      $isset->start = $raw[4];
                      $isset->end = $raw[5];
                      $isset->role = $raw[6];
                      $isset->link = $raw[7];
                      $isset->skills = $raw[8];
                      $isset->attachments = $raw[9];
                      $isset->type = $raw[10];
                      $isset->active = $raw[11];
                      $isset->sort = $raw[12];
                      $isset->updated_at = $raw[13];
                      $isset->created_at = $raw[14];

                      $isset->save(false);

                      $projects = Projects::find()->asArray()->all();









                  }

          }

        }

        $projects = Projects::find()->all();
        foreach($projects as $project)
        {
           if(!in_array($project->id, $ids))
           {
               $project->delete();
           }

        }

    }
    
    
    public function actionIndex()
    {


        return $this->render('index', [

        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Projects();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
